Vue 3のチュートリアルを試して見ます。  
  
## Workspaces
https://gitpod.io/workspaces/

## サーバー終了方法  
Win+Shift+@
npx kill-port 8080  

# learnvue3

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
